wget https://download.jetbrains.8686c.com/python/pycharm-community-2018.1.4.tar.gz;
tar -xzvf pycharm-community-2018.1.4.tar.gz;
mkdir ~/devtools;
cp -r pycharm-community-2018.1.4 ~/devtools/pycharm-community-2018.1.4/;

rm -rf pycharm-community-2018.1.4;
rm -f pycharm-community-2018.1.4.tar.gz;

echo "export PATH=~/devtools/pycharm-community-2018.1.4/bin:${PATH};" >> ~/.bashrc;

