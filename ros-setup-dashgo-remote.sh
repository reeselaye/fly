# 首先将 ssh 公钥添加到 reeselaye@gitlab.com。

# for DashgoD1 remote
sudo apt-get install git python-serial ros-kinetic-serial g++ ros-kinetic-turtlebot-rviz-launchers ros-kinetic-teleop-twist-keyboard ros-kinetic-move-base-msgs ros-kinetic-navigation ros-kinetic-slam-gmapping ros-kinetic-teb-local-planner -y;
sudo apt-get install libghc-sdl-image-dev libsdl-image1.2-dev -y;
sudo apt-get install ros-kinetic-world-canvas-server ros-kinetic-rosbridge-server ros-kinetic-robot_pose_publisher python-warehouse python-redis -y;
sudo pip install warehouse_ros redis;

mkdir ~/sources;
git clone --recurse-submodules git@gitlab.com:reeselaye/dashgo-remote.git ~/sources/dashgo_remote/;
cd ~/sources/dashgo_remote;
catkin_make;

# for DashgoD1 remote
echo "source ~/sources/dashgo_remote/devel/setup.bash;" >> ~/.bashrc;
echo "export ROS_HOSTNAME=ubuntu;" >> ~/.bashrc;
echo "export ROS_IP=192.168.31.29;" >> ~/.bashrc;
echo "export ROS_MASTER_URI=http://192.168.31.200:11311;" >> ~/.bashrc;
echo "export ROS_PACKAGE_PATH=~/sources/dashgo_remote/:/opt/ros/kinetic/share;" >> ~/.bashrc;

