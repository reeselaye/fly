# 首先将 ssh 公钥添加到 reeselaye@gitlab.com。

sudo apt-get update;
sudo apt-get install screen expect -y;

# for DashgoD1 local
sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential -y;
sudo usermod -a -G dialout reese;
sudo apt-get install git python-serial ros-kinetic-serial g++ ros-kinetic-teleop-twist-keyboard -y;
# sudo apt-get install ros-kinetic-turtlebot-rviz-launchers -y; librealsense error
sudo apt-get install ros-kinetic-world-canvas-server ros-kinetic-rosbridge-server ros-kinetic-robot_pose_publisher python-warehouse python-redis -y;
sudo pip install warehouse_ros redis;

rm -rf ~/sources/dashgo_local;
mkdir ~/sources;
git clone --recurse-submodules git@gitlab.com:reeselaye/dashgo-local.git ~/sources/dashgo_local/;
cd ~/sources/dashgo_local;
# sudo chmod +x make_local.sh;
# sudo ./make_local.sh;
chmod +x make_and_run_ros_mock_nav_services.sh;
./make_and_run_ros_mock_nav_services.sh;

