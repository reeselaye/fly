sudo sh -c 'echo "deb http://mirrors.ustc.edu.cn/ros/ubuntu/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -


# Configure your Ubuntu repositories to allow "restricted," "universe," and "multiverse." You can follow the Ubuntu guide for instructions on doing this.

# sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu xenial main" > /etc/apt/sources.list.d/ros-latest.list';
# sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116;

sudo apt-get update;
sudo apt-get install ros-kinetic-desktop-full -y;
sudo rosdep init;
rosdep update;

sudo apt-get install screen expect -y;
sudo apt-get install python-pip -y;
sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential -y;
sudo usermod -a -G dialout actor;
sudo apt-get install git python-serial ros-kinetic-serial g++ ros-kinetic-teleop-twist-keyboard -y;
sudo apt-get install ros-kinetic-world-canvas-server ros-kinetic-rosbridge-server ros-kinetic-robot-pose-publisher -y;
sudo apt-get install python-redis -y;
sudo pip install redis rospy_message_converter mysql-connector;

sudo apt-get install ros-kinetic-turtlebot-rviz-launchers ros-kinetic-move-base-msgs ros-kinetic-navigation ros-kinetic-slam-gmapping ros-kinetic-teb-local-planner -y;

sudo apt-get install ros-kinetic-rosjava -y;
sudo apt-get install mongodb -y;
sudo apt-get install ros-kinetic-map-server ros-kinetic-amcl ros-kinetic-move-base ros-kinetic-robot-pose-publisher -y
sudo apt-get install ros-kinetic-warehouse-ros ros-kinetic-rospy-message-converter  -y
sudo apt-get install mongodb -y



# sudo apt-get install libghc-sdl-image-dev libsdl-image1.2-dev -y;
sudo apt-get install openssh-server -y;


sudo apt-get install ros-kinetic-yocs-velocity-smoother -y
sudo apt-get install ros-kinetic-world-canvas-server -y
sudo pip install unique_id


sudo apt-get install ros-kinetic-warehouse-ros-mongo -y
sudo apt-get install ros-kinetic-warehouse-ros -y
sudo apt-get install ros-kinetic-robot-pose-ekf -y
sudo pip install pymongo
sudo apt-get install ros-kinetic-unique-id -y

echo "source /opt/ros/kinetic/setup.bash;" >> ~/.bashrc;
source ~/.bashrc;

# test
echo "Testing ROS installation ...";
roscore;

